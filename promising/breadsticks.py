import re
import sys
from redbaron import RedBaron
from redbaron.nodes import AssignmentNode
from redbaron.nodes import CommentNode
from redbaron.nodes import ImportNode

FUNC_TAG = "_parallelized"

curFuncNum = 0
curParamNum = 0

'''
Calculates the number of spaces in front of a line
'''
def numLeadingWhite(l):
    num = 0
    for c in l:
        if c == ' ':
            num += 1
        else:
            return num
'''
Checks if line contains a function fall that should be parallelized
'''
def isFunctionCallToParallelize(ast, ndx):
    return ndx > 0 and type(ast[ndx]) == AssignmentNode \
        and type(ast[ndx - 1]) == CommentNode \
        and '@Parallel' in ast[ndx - 1].value \
        and ast[ndx].find('CallNode') != None

'''
Extracts info on a function call from a line in the AST
    1. Modifies call to pass process names
    2. Adds calls to start processes
    3. Defines temporary functions for each argument in the call
'''      
def extractCallInfo(ast, ndx):
    global curParamNum
    global curFuncNum
    callNode = ast[ndx].find('CallNode')
    params = [e.value for e in callNode]
    name = callNode.parent[0].value
    callNode.parent[0].value = callNode.parent[0].value + FUNC_TAG
      
    pNames = {}
    starts = []
    ndx = 0
    tempFuncs = []
    for p in params:
        # get variables in parameter
        pVars = [str(x) for x in p.find_all('CallArgumentNode')]
        if len(pVars) == 0:
            pVars.append(str(p))
        
        # rename parameters
        pNames[p] = "Param" + str(curParamNum)
        callNode[ndx] = "'" + pNames[p] + "'"
        ndx += 1
        curParamNum += 1
        
        # process start lines
        pFuncName = "ParamFunc" + str(curFuncNum)
        curFuncNum += 1
        pString = ', '.join(pVars)
        if len(pVars) == 1:
            pString += ','
        starts.append("prom.start('{}', {}, ({}))".format(pNames[p], \
            pFuncName, pString))
        
        # temp functions
        tempFunc = 'def {}({}):\n    '.format(pFuncName, ', '.join(pVars))
        tempFunc += 'return ' + str(p)
        tempFuncs.append(tempFunc)
        
    return name, params, starts, tempFuncs

'''
Makes a parallelized version of a called function.
    1. Called [original name]_parallelized(...)
    2. Replaces variables with names of processes containing the variables
'''
def makeNewFunction(ast, funcName):
    func = ast.find('DefNode', name=funcName)
    notdone = [str(a) for a in func.arguments]
        
    lines = func.dumps().split('\n')
    lines[0] = lines[0].replace(func.name, func.name + FUNC_TAG)
    for ndx in range(1, len(lines)):
        for arg in notdone:
            # TODO
            #pat = re.compile("(?: |\(|\t)" + arg + "(?: |\.|\t|\))")
            if arg in lines[ndx]: #pat.match(lines[ndx]):
                waitLine = '{} = prom.wait({})'.format(arg, arg)
                lines.insert(ndx, numLeadingWhite(lines[ndx])*' ' + waitLine)
                notdone.remove(arg)
    
    return func.name, [x for x in lines if len(x.strip()) > 0]

'''
Performs the translation of a Python file. Attempts to parallelize all
function calls that have @Parallel before them.
'''
def doTranslation(fname, outname):
    with open(fname, 'r') as p:
        ast = RedBaron(p.read())
    # find function calls to parallelize
    newFuncs = {}
    miniFuncs = []
    name = None
    for ndx in range(len(ast)):
        if isFunctionCallToParallelize(ast, ndx):
            name, params, starts, tempFuncs = extractCallInfo(ast, ndx)
            miniFuncs += tempFuncs
            print('\tParallelizing call to:', name)
            oname, newFunc = makeNewFunction(ast, name)
            newFuncs[oname] = newFunc
            # add start lines
            for st in starts[::-1]:
                ast[ndx].insert_before(st)
    print()
    if name != None:
        # insert param functions
        ndx = 0
        while type(ast[ndx]) == ImportNode:
            ndx += 1
        for pFunc in tempFuncs[::-1]:
            ast[ndx].insert_before(pFunc)
        ast[ndx].insert_before('\n')
        
        # insert import
        ast[0].insert_before('prom = Promiser()')
        ast[0].insert_before('from Promise import Promiser')
        
        # insert new functions
        out = ast.dumps().split('\n')
        for func in newFuncs:
            for ndx in range(len(out)):
                if 'def ' + func + '(' in out[ndx]:
                    out = out[:ndx] + newFuncs[func] + [' '] + out[ndx:]
                    break
        with open(outname, 'w+') as f:
            f.write('\n'.join(out))

if __name__ == '__main__':
    if len(sys.argv) < 2 or '.py' not in sys.argv[1]:
        print('Usage: python3 function_converter.py [python file name]')
        print('\tAdd "@Parallel" before a function call that should be parallelized')
        exit(-1)
    infile = sys.argv[1]
    print('Blindly parallelizing function calls in:', infile)
    doTranslation(infile, infile.replace('.py', '-out.py'))
    print('Done')
    
