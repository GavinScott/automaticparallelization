import urllib.request
import sys

urls = [
    'https://en.wikipedia.org/wiki/Main_Page',
    'https://en.wikipedia.org/wiki/John_Sherman',
    'https://en.wikipedia.org/wiki/Republican_Party_(United_States)',
    'https://en.wikipedia.org/wiki/Assassination_of_Abraham_Lincoln',
    'https://en.wikipedia.org/wiki/Andrew_Johnson',
    'https://en.wikipedia.org/wiki/Governor_of_Tennessee',
    'https://en.wikipedia.org/wiki/Tennessee',
    'https://en.wikipedia.org/wiki/Cherokee_language',
    'https://en.wikipedia.org/wiki/Polysynthetic_language'    
]

content = {}
for url in urls:
    content[url] = urllib.request.urlopen(url).read()

print('\n\n'.join(str(content[url]) for url in urls))
