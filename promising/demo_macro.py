[
        Transform({'itr': 'identifier', 'src': 'identifier'},
            '[urllib.request.urlopen(itr).read() for itr in src]',
            """[x.get() for x in
                 [lazy_call(lambda y: urllib.request.urlopen(y).read(), (itr, ))
                  for itr in src]]"""),
        Transform({'condition': 'any', 'ifcase': 'any', 'elsecase': 'any'},
            'cond(condition, ifcase, elsecase)',
            '(ifcase if condition else elsecase)')
]
