import unittest
import sys
from redbaron import RedBaron

def is_parallelizable(expr):
    """
    Is the given expression something that can be safely parallelized?
    This will probably have to be a whitelist somehow with some pattern
    matching system because right now it only kind of checks for
    urllib.request.urlopen(something).read()
    """
    if (expr.value[0].value == 'urllib' and
            expr.value[1].value == 'request' and
            expr.value[2].value == 'urlopen' and
            expr.value[4].value == 'read'):
        return True
    return False

def parallelize_comprehension(compr):
    """
    Take the given list comprehension and create one that starts many
    subprorcesses and waits for them in parallel

    given any expression of the form
    [<expression> for <idenfitier_a> in <identifier_b>]
    return something of the form
    [x.get() for x in
     [lazy_call(lambda <identifier_a>: <expression>, (<identifier_a>, ))
      for <identifier_a> in <identifier_b>]]
    """
    loop_itr = compr.generators[0].iterator.value
    loop_op = str(compr.result)
    loop_gen = str(compr.generators[0])
    functor = "lambda %s: %s" % (loop_itr, loop_op)

    res = RedBaron("[x.get() for x in [lazy.lazy_call(%s, (%s, )) %s]]"
                   % (functor, loop_itr, loop_gen))[0]

    compr.result, compr.generators = res.result, res.generators

def add_import(ast):
    """
    Add `import lazy` to the top of this ast
    """
    ast[0].insert_before('import lazy')

def main(inFile, outFile):
    """
    Parallelize the contents of |inFile| and write the output to |outFile|
    """
    with open(inFile, 'r') as p:
        ast = RedBaron(p.read())

    add_import(ast)
    for compr in ast.find_all('ListComprehensionNode'):
        if is_parallelizable(compr.result):
            parallelize_comprehension(compr)

    with open(outFile, 'w') as p:
        p.write(ast.dumps())

class TestStuff(unittest.TestCase):
    def test_something(self):
        assert(True)
    def test_parallelize_urlopenread(self):
        inStr = '[urllib.request.urlopen(url).read() for url in urls]'
        expr = RedBaron(inStr)[0]
        parallelize_comprehension(expr)
        assert(str(expr) ==
                '[x.get() for x in [lazy.lazy_call(lambda url: urllib.request.urlopen(url).read(), (url, ))  for url in urls]]')

        # try same thing with other identifiers
        inStr = '[urllib.request.urlopen(oops).read() for oops in oopsies]'
        expr = RedBaron(inStr)[0]
        parallelize_comprehension(expr)
        assert(str(expr) ==
                '[x.get() for x in [lazy.lazy_call(lambda oops: urllib.request.urlopen(oops).read(), (oops, ))  for oops in oopsies]]')

if __name__ == '__main__' and len(sys.argv) == 1:
    print("Calling with no arguments initiates unit tests")
    unittest.main()
elif __name__ == '__main__' and len(sys.argv) == 3:
    print("Converting file")
    inFile = sys.argv[1]
    outFile = sys.argv[2]
    try:
        main(inFile, outFile)
        print("success")
    except FileNotFoundError:
        print("Could not find file: %s" % (inFile, ))
