import urllib.request

def compare(urls, html1, html2):
    content = [urllib.request.urlopen(url).read() for url in urls]
    return sum([len(c) for c in content]), len(html1) > len(html2)


urls = [
        'https://en.wikipedia.org/wiki/Main_Page',
        'https://en.wikipedia.org/wiki/John_Sherman',
        'https://en.wikipedia.org/wiki/Republican_Party_(United_States)'
]
 
url1 = 'https://en.wikipedia.org/wiki/Main_Page'
url2 = 'https://en.wikipedia.org/wiki/Manchuria'

# @Parallel
x = 5

# @Parallel
l, out = compare(urls, urllib.request.urlopen(url1).read(), urllib.request.urlopen(url2).read())

print('URL content lengths:', l)
print('Page 1 longer?', out)
