import sys
import subprocess
import time
import itertools
import os
from redbaron import RedBaron
from dominos import Transform

NUM_LOOP = 0
INDENT = 4
STATS_FILE = '-stats.txt'
TEMP_FILE = '-temp.py'
OUT_FILE = '-out.py'

curTime = lambda: int(round(time.time() * 1000))

def numLeadingWhite(l):
    num = 0
    for c in l:
        if c == ' ':
            num += 1
        else:
            return num

def avg(li):
    try:
        return sum(li) / len(li)
    except:
        return -1

def translate(p):
    lines = [x.replace('\n', '') for x in p.readlines()]
    loops = findLoops(lines)
    out = 'from Promise import Promiser\nprom = Promiser()\n\n'
    ndx = 0
    while lines[ndx][:6] == 'import':
        out += lines[ndx] + '\n'
        ndx += 1
    out += '\n'
    for loop in loops:
        out += loop.func
    out += '\n'.join(lines[ndx:])
    for loop in loops:
        out = out.replace(loop.text, loop.newText)
    return out

def findLoops(lines):
    ast = RedBaron('\n'.join(lines))

    loops = []
    for mark in ast.find_all('CommentNode', value='# @Parallel'):
        loops.append(mark.absolute_bounding_box.top_left.line + 2)

    print('Identified', len(loops), 'loops')
    return loops
            
def runProg(fname, args):
    ret = 0
    try:
        t1 = curTime()
        out = subprocess.check_output([sys.executable, fname, *args])
    except subprocess.CalledProcessError as grepexc:
        # catch non-zero error code
        out = grepexc.output
        ret = grepexc.returncode
    t2 = curTime()
    return out, ret, (t2 - t1)

def tryLoops(lines, loops):
    ast = RedBaron('\n'.join(lines))
    ast[0].insert_before('from lazy import lazy_call')
    transform = Transform({'func': 'any', 'itr': 'identifier', 'gen': 'any'},
          "[func for itr in gen]",
          "[x.get() for x in [lazy_call(lambda itr: func, (itr, )) for itr in gen]]")

    ast = transform.apply_subset(ast, loops)
    
    return ast.dumps()

def trial(tempName, lines, loops, argList, outputs, returns):
    print('\ttrying: ', loops)
    # save temporary file
    with open(tempName, 'w+') as temp:
        outPy = tryLoops(lines, loops)
        temp.write(outPy)
    # run temp file and check output
    times = []
    for args in argList:
        valid = True
        tempOut, tempRet, tempTime = runProg(tempName, args)
        # check output
        if tempOut != outputs[str(args)] or tempRet != returns[str(args)]:
            valid = False
            break
        times.append(tempTime)
    if valid:
        print('\t\tsuccess. time: ' + str(avg(times)))
    else:
        print('\t\tfailure.')
    return valid, avg(times)

def doTranslation(fname, argList, outputs, returns, times):
    """
    Attempt to optimize different combinations of loops in this program
    """
    with open(fname, 'r') as p:
        lines = [x.replace('\n', '') for x in p.readlines()]
    # find all possible parallelization spots
    loops = findLoops(lines)
    print("Loops on lines: ", loops)
    tempName = '.' + inName[:-3] + TEMP_FILE

    # test individual loops
    validOptimizations = []

    tempBest = None
    tempBestTime = None

    i = 0
    for loop in loops:
        valid, t = trial(str(i) + tempName, lines, [loop], argList, outputs, returns)
        if valid:
            validOptimizations.append(loop)
        if valid and (tempBest == None or t < tempBestTime):
            tempBest = [loop]
            tempBestTime = t
        i += 1

    if tempBest == None:
        print('Sorry, we can\'t find any way to parallelize this.')
        exit(0)

    # test combinations of loops
    for i in range(2, len(validOptimizations) + 1):
        for combo in itertools.combinations(validOptimizations, i):
            valid, t = trial(tempName, lines, combo, argList, outputs, returns)
            if valid and t < tempBestTime:
                tempBest = combo
                tempBestTime = t
    
    # delete temp file
    os.remove(tempName)
    
    # generate best output
    outPy = tryLoops(lines, tempBest)
    # generate stats
    origTime = avg([times[a] for a in times])
    timePerc = (tempBestTime / origTime) * 100
    stats = '\nSTATS:\n'
    stats += '\tNumber of areas parallelized: ' + str(len(tempBest)) + '\n'
    stats += '\tNew average running time: ' + str(tempBestTime) + ' ms\n'
    stats += '\tNew average running time is {0:.2f}% of original\n'.format(timePerc)
    
    # print stats to screen
    print(stats)
    
    return outPy, stats

if __name__ == '__main__':
    validIn = len(sys.argv) > 1 and '.py' in sys.argv[1]
    # parse input
    if not validIn or '-h' in sys.argv or '-help' in sys.argv:
        print('Usage: [filename] ["args 1", "args 2", ...]')
        exit(0)
    inName = sys.argv[1]
    args = [[inName, *x.split()] for x in sys.argv[2:]]
    
    # get original program outputs and return codes
    outputs = {}
    returns = {}
    times = {}
    print('\nRunning original program with given inputs')
    for arg in args:
        print('\tArgs:', arg)
        try:
            out, ret, t = runProg(inName, arg)
            outputs[str(arg)] = out
            returns[str(arg)] = ret
            times[str(arg)] = t
        except Exception as e:
            print('Error - Original code throws exception with :', arg)
            exit(-1)
    print('Original avg time:', avg([times[a] for a in times]))
    
    # do translation
    print('\nAttempting parallelization...')
    out, stats = doTranslation(inName, args, outputs, returns, times)
    
    # output results
    outFname = inName[:-3] + OUT_FILE
    statsFname = inName[:-3] + STATS_FILE
    print('\nOutputting translated python file:\t', outFname)
    with open(outFname, 'w+') as outFile:
        outFile.write(out)
    print('Outputting stats file:\t\t\t', statsFname)
    with open(statsFname, 'w+') as statsFile:
        statsFile.write(stats)
