import multiprocessing as mp

class Promiser():
    def __init__(self):
        self.procs = mp.Manager().dict()

    def start(self, name, tgt, params):
        mp.Process(target=self._wrapper, args=(name, self.procs, tgt, params)).start()

    def _wrapper(self, name, d, tgt, params):
        d[name] = tgt(*params)

    def check(self, name):
        return name in self.procs
        
    def wait(self, name):
        while name not in self.procs:
            pass
        return self.procs[name]
