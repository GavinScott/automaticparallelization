import urllib.request
import sys
from lazy import *

urls = [
    'https://en.wikipedia.org/wiki/Main_Page',
    'https://en.wikipedia.org/wiki/John_Sherman',
    'https://en.wikipedia.org/wiki/Republican_Party_(United_States)',
    'https://en.wikipedia.org/wiki/Assassination_of_Abraham_Lincoln',
    'https://en.wikipedia.org/wiki/Andrew_Johnson',
    'https://en.wikipedia.org/wiki/Governor_of_Tennessee',
    'https://en.wikipedia.org/wiki/Tennessee',
    'https://en.wikipedia.org/wiki/Cherokee_language',
    'https://en.wikipedia.org/wiki/Polysynthetic_language'    
]

content = [lazy_call(lambda x: urllib.request.urlopen(x).read(), (url,))
            for url in urls]
content = [x.get() for x in content]

lowered = [html.lower() for html in content]


if len(sys.argv) > 1:
    print('Args:', sys.argv[1:])
print('\n\n'.join(str(c) for c in lowered))

if '-q' in sys.argv:
    sys.exit(7)
