import urllib.request
from lazy import lazy_call

urls = [
    'https://en.wikipedia.org/wiki/Main_Page',
    'https://en.wikipedia.org/wiki/John_Sherman',
    'https://en.wikipedia.org/wiki/Republican_Party_(United_States)',
    'https://en.wikipedia.org/wiki/Assassination_of_Abraham_Lincoln',
    'https://en.wikipedia.org/wiki/Andrew_Johnson',
    'https://en.wikipedia.org/wiki/Governor_of_Tennessee',
    'https://en.wikipedia.org/wiki/Tennessee',
    'https://en.wikipedia.org/wiki/Cherokee_language',
    'https://en.wikipedia.org/wiki/Polysynthetic_language',
    'https://en.wikipedia.org/wiki/Imperial_Japanese_Navy',
    'https://en.wikipedia.org/wiki/Empire_of_Japan',
    'https://en.wikipedia.org/wiki/Japanese_colonial_empire',
    'https://en.wikipedia.org/wiki/Manchuria',
    'https://en.wikipedia.org/wiki/Japanese_people'
]

x = 0
def incX():
    global x
    x = x + 1
    return x

content = [urllib.request.urlopen(url).read() for url in urls]
lowered = [html.lower() for html in content]
inc = [incX() for z in urls]

content_b = [urllib.request.urlopen(url2).read() for url2 in urls]

def do_something():
    print("boo!")

def do_somethingelse():
    print("yay!")

a = cond(False, do_something(), do_somethingelse())

