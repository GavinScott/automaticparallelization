import multiprocessing as mp

def _subprocess(out_pipe, func, params):
    """
    Execute the given function and send the result through the given
    unix pipe.
    """
    ret_val = func(*params)
    out_pipe.send(ret_val)

class _lazyFunc(object):
    """
    executes |func| in its own process.  |func| does not block until the
    value is needed (hopefully |func| is already done by that point though)

    """
    def __init__(self, func, args):
        """
        Hopefulle the result of calling _subprocess is something that can
        be sent through a unix pipe O:
        """
        self.recv_pipe, self.send_pipe = mp.Pipe(False)
        self.ret_val = None
        self.finished = False
        self.p = mp.Process(target=_subprocess,
                            args=(self.send_pipe, func, args))
        self.p.start()

    def get(self):
        """
        Block the process on the pipe rather than doing a busywait
        (free up cpu to work on subprocess)
        """
        if not self.finished:
            self.ret_val = self.recv_pipe.recv()
            self.finished = True
        return self.ret_val

    def __del__(self):
        """
        If this object goes out of scope (or is otherwise deleted), kill
        subprocess so the program can exit properly.
        """
        self.p.terminate()

def lazy_call(func, args):
    return _lazyFunc(func, args)

def parallel_map(func, data):
    return [x.get() for x in
            [lazy_call(func, (args, )) for args in data]]

import urllib.request
def read_url(url):
    return urllib.request.urlopen(url).read()

l = lazy_call(read_url, ("https://wikipedia.org/",))

urls = [
    'https://en.wikipedia.org/wiki/Main_Page',
    'https://en.wikipedia.org/wiki/John_Sherman',
    'https://en.wikipedia.org/wiki/Republican_Party_(United_States)',
    'https://en.wikipedia.org/wiki/Assassination_of_Abraham_Lincoln',
    'https://en.wikipedia.org/wiki/Andrew_Johnson',
    'https://en.wikipedia.org/wiki/Governor_of_Tennessee',
    'https://en.wikipedia.org/wiki/Tennessee',
    'https://en.wikipedia.org/wiki/Cherokee_language',
    'https://en.wikipedia.org/wiki/Polysynthetic_language'    
]

#l = [lazy_call(read_url, (url,)) for url in urls]
#l = [x.get() for x in l]
l = parallel_map(read_url, urls)
