import sys
from redbaron import RedBaron
from dominos import Transform

def do_conversion(macro_def_fname, input_fname):
    with open(macro_def_fname, 'r') as macro_def_file:
        with open(input_fname, 'r') as input_file:
            ast = RedBaron(input_file.read())
            macro_def = eval(macro_def_file.read(), {"Transform": Transform})
            for macro in macro_def:
                ast = macro.apply(ast)
    return ast.dumps()

if __name__ == '__main__':
    if len(sys.argv) == 3:
        print(do_conversion(sys.argv[1], sys.argv[2]))
    else:
        print("Usage: ./macro.py macro_description_file.py in_file.py")
