import urllib.request
import sys

urls = [
    'https://en.wikipedia.org/wiki/Main_Page',
    'https://en.wikipedia.org/wiki/John_Sherman',
    'https://en.wikipedia.org/wiki/Republican_Party_(United_States)',
    'https://en.wikipedia.org/wiki/Assassination_of_Abraham_Lincoln',
    'https://en.wikipedia.org/wiki/Andrew_Johnson',
    'https://en.wikipedia.org/wiki/Governor_of_Tennessee',
    'https://en.wikipedia.org/wiki/Tennessee',
    'https://en.wikipedia.org/wiki/Cherokee_language',
    'https://en.wikipedia.org/wiki/Polysynthetic_language',
    'https://en.wikipedia.org/wiki/Imperial_Japanese_Navy',
    'https://en.wikipedia.org/wiki/Empire_of_Japan',
    'https://en.wikipedia.org/wiki/Japanese_colonial_empire',
    'https://en.wikipedia.org/wiki/Manchuria',
    'https://en.wikipedia.org/wiki/Japanese_people'
]

x = 1
def incX():
    global x
    x *= 2

# @Parallel
content = [urllib.request.urlopen(url).read() for url in urls]

# @Parallel
lowered = [html.lower() for html in content]

# @Parallel
uppered = [html.upper() for html in content]

# @Parallel
inc = [incX() for z in urls]

# @Parallel
uppercontent = [
        urllib.request.urlopen(url).read().upper()
        for url in urls]

print(content)

print('X:', x)
if len(sys.argv) > 1:
    print('Args:', sys.argv[1:])
print('\n\n'.join(str(c) for c in lowered))

if '-q' in sys.argv:
    sys.exit(7)
