import time
from Promise import Promiser
import urllib.request
import sys

def pull(url):
    return urllib.request.urlopen(url).read()

prom = Promiser()

urls = [
    'https://en.wikipedia.org/wiki/Main_Page',
    'https://en.wikipedia.org/wiki/John_Sherman',
    'https://en.wikipedia.org/wiki/Republican_Party_(United_States)',
    'https://en.wikipedia.org/wiki/Assassination_of_Abraham_Lincoln',
    'https://en.wikipedia.org/wiki/Andrew_Johnson',
    'https://en.wikipedia.org/wiki/Governor_of_Tennessee',
    'https://en.wikipedia.org/wiki/Tennessee',
    'https://en.wikipedia.org/wiki/Cherokee_language',
    'https://en.wikipedia.org/wiki/Polysynthetic_language'    
]

print('Usage:\t"python3 driver.py" for sequential\n\t"python3 driver.py [anything]" to use promiser\n')
if len(sys.argv) > 1:
    print('Using promiser')
    content = {}
    for url in urls:
        #content[url] = None
        prom.start(url, pull, (url,))

    for url in urls:
        content[url] = prom.wait(url)
else:
    print('NOT using promiser')
    content = {}
    for url in urls:
        content[url] = urllib.request.urlopen(url).read()
        
print('done')
