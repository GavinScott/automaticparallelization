import re
import sexpdata

LAMBDA = 'λ'

def repList(li):
    if type(li) == list:
        if len(li) == 1:
            return li[0]
        else:
            return [repList(x) for x in li]
    else:
        return li

def translate(program):
    program = program.replace('lambda', LAMBDA)
    sexp = sexpdata.loads(program)
    if type(sexp) == int:
        return sexp
    
    translated = []
    i = 0
    while i in range(len(sexp)):
        if i < len(sexp) - 1 and type(sexp[i]) == sexpdata.Symbol and type(sexp[i + 1]) == list:
            translated.append([sexp[i], sexp[i + 1]])
            i += 2
        else:
            translated.append(sexp[i])
            i += 1
    return ' '.join(pretty(toVals([repList(x) for x in translated])).split())

def pretty(vals):
    if type(vals) == list:
        return '(' + ' '.join([pretty(x) for x in vals]) + ')'
    elif vals != ':':
        return str(vals)
    return ''

def toVals(sexp):
    if type(sexp) == list:
        return [toVals(x) for x in sexp]
    elif type(sexp) == sexpdata.Symbol:
        return sexp.value()
    else:
        return sexp

def test(program):    
    print('IN:\t', program)
    print('OUT:\t', translate(program))
    print()    

if __name__ == '__main__':
    test('5')
    test('(lambda x : 5)')
    test('(x(2))')
    test('(lambda x : x)')
    test('(lambda y : y(2))')
    test('(lambda y : y(y(2)))')
    test('((lambda y : y) 4)')
    
    print('--- This next one is wrong :( ---\n')
    test('((lambda x: (lambda y: x(y)))(lambda z: z)(6))')
    
