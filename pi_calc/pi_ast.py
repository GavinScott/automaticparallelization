class Program(object):
    def __init__(self, val):
        self.val = val
    def __str__(self):
        return "new(Gout).%s" % str(self.val)

class Tuple(object):
    def __init__(self, l, r):
        self.l = l
        self.r = r
    def __str__(self):
        return '(%s,%s)' % (self.l, self.r)

class Value(object):
    def __init__(self, val):
        self.val = val
    def __str__(self):
        return "%s" % self.val

class New(object):
    def __init__(self, channel_name, rest):
        assert(type(channel_name) == str)
        self.channel_name = channel_name
        self.rest = rest
    def __str__(self):
        return "new(%s).%s" % (self.channel_name, self.rest)

class Read(object):
    def __init__(self, channel_name, variable_name, rest):
        assert(type(channel_name) == str)
        self.channel_name = channel_name
        self.variable_name = variable_name
        self.rest = rest
    def __str__(self):
        return "%s?%s.%s" % (self.channel_name, self.variable_name, self.rest)

class Write(object):
    def __init__(self, channel_name, variable_name, rest):
        assert(type(channel_name) == str)
        self.channel_name = channel_name
        self.variable_name = variable_name
        self.rest = rest
    def __str__(self):
        return "%s!%s.%s" % (self.channel_name, self.variable_name, self.rest)

class SpawnForever(object):
    def __init__(self, process, rest):
        self.process = process
        self.rest = rest
    def __str__(self):
        return "&[%s].%s" % (
                self.process,
                self.rest)

class Spawn(object):
    def __init__(self, process, rest):
        self.process = process
        self.rest = rest
    def __str__(self):
        return "[%s].%s" % (
                self.process,
                self.rest)

class End(object):
    def __init__(self):
        pass
    def __str__(self):
        return "0"

"""
print(New('in',
        New('out',
            Spawn(
                Write('out', '5', End()),
                Write('parent', 'in',
                    Write('parent', 'out', End())),
                True))))
"""
