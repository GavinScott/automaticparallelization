import unittest

class Program(object):
    def __init__(self, prog):
        self.val = prog

    def __eq__(self, other):
        return type(self) == type(other) and self.val == other.val

    def __str__(self):
        return str(self.val)

class BinOp(object):
    OPERATORS = set(['+', '-', '=', '<'])
    def __init__(self, operation, left, right):
        self.op = operation
        self.left = left
        self.right = right

    def __eq__(self, other):
        return (type(self) == type(other)
                and self.op == other.op
                and self.left == other.left
                and self.right == other.right)

    def __str__(self):
        return '(%s %s %s)' % (str(self.op), str(self.left), str(self.right))

class Number(object):
    def __init__(self, val):
        self.val = val

    def __eq__(self, other):
        return type(self) == type(other) and self.val == other.val

    def __str__(self):
        return str(self.val)

class Identifier(object):
    def __init__(self, name):
        self.val = name

    def __eq__(self, other):
        return type(self) == type(other) and self.val == other.val

    def __str__(self):
        return str(self.val)

class Definition(object):
    def __init__(self, arg, body):
        self.arg = arg
        self.body = body

    def __eq__(self, other):
        return (type(self) == type(other)
                and self.arg == other.arg
                and self.body == other.body)

    def __str__(self):
        return '(λ %s. %s)' % (str(self.arg), str(self.body))

class Application(object):
    def __init__(self, function, argument):
        self.function = function
        self.argument = argument

    def __eq__(self, other):
        return (type(self) == type(other)
                and self.function == other.function
                and self.argument == other.argument)

    def __str__(self):
        return '(%s %s)' % (str(self.function), str(self.argument))


class ParserTest(unittest.TestCase):
    def test_num(self):
        self.assertTrue(Number(4) == Number(4))
        self.assertFalse(Number(5) == Number(4))
        self.assertFalse(Identifier('a') == Number(4))

    def test_prog(self):
        self.assertTrue(Program(Number(5)) == Program(Number(5)))
        self.assertFalse(Program(Number(5)) == Program(Number(4)))

    def test_identifier(self):
        self.assertTrue(Identifier('apple') == Identifier('apple'))
        self.assertFalse(Identifier('apple') == Identifier('banana'))

    def test_str(self):
        self.assertEqual(str(Identifier('apple')), 'apple')
        self.assertEqual(
                str(Application(
                    Definition(Identifier('x'), Identifier('x')),
                    Number(5))),
                '((λ x. x) 5)')

if __name__ == '__main__':
    unittest.main()
