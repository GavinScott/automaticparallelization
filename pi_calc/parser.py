import sexpdata
from lambda_ast import *
import unittest

def parse(program):
    "Take program as a string and output a lambda ast"
    return Program(_parse(sexpdata.loads(program)))

lambdas = set(['lambda', '^', 'λ'])

def _parse(sexp):
    "Take a program as an s-expression and outpu a lambda ast"
    if (type(sexp) == list and len(sexp) == 3
            and type(sexp[0]) == sexpdata.Symbol):
        if sexp[0].value() in lambdas:
            return Definition(_parse(sexp[1]), _parse(sexp[2]))
        if sexp[0].value() in BinOp.OPERATORS:
            return BinOp(sexp[0].value(), _parse(sexp[1]), _parse(sexp[2]))
    elif type(sexp) == list and len(sexp) == 2:
        return Application(_parse(sexp[0]), _parse(sexp[1]))
    elif type(sexp) == int:
        return Number(sexp)
    elif type(sexp) == sexpdata.Symbol and sexp.value() not in lambdas:
        return Identifier(sexp.value())
    else:
        raise "Syntax error"

class ParserTest(unittest.TestCase):
    def test_num(self):
        self.assertEqual(parse('5'), Program(Number(5)))
        self.assertEqual(parse('(5 5)'),
                Program(Application(Number(5), Number(5))))
        self.assertEqual(parse('(lambda x x)'),
                Program(Definition(Identifier('x'), Identifier('x'))))
        self.assertEqual(parse('(+ 4 3)'),
                Program(BinOp('+', Number(4), Number(3))))

    def test_str(self):
        self.assertEqual(str(parse('((lambda x x) 5)')),
                '((λ x. x) 5)')

if __name__ == '__main__':
    unittest.main()
