import parser
import lambda_ast as lam
import pi_ast as pi
import unittest
from collections import defaultdict

def translate_program(program):
    return str(translate_term(parser.parse(program), 'Gout'))

chans = defaultdict(lambda: 0)
def new_channel(prefix):
    global chans
    postfix = chans[prefix]
    chans[prefix] += 1
    return "%s_%s" % (prefix, postfix)

def newall(channels, rest):
    "helper function to define muleiple channels at once"
    if channels:
        return pi.New(channels[0],
                newall(channels[1:], rest))
    else:
        return rest

def spawnall(tasks, rest):
    "helper function to spawn multiple processes at once"
    if tasks:
        return pi.Spawn(tasks[0],
                newall(tasks[1:], rest))
    else:
        return rest


def translate_term(l_ast, parent):
    if type(l_ast) == lam.Program:
        return pi.Program(
                translate_term(l_ast.val, parent))
    if type(l_ast) == lam.Number:
        return pi.Write(parent, l_ast.val, pi.End())
    if type(l_ast) == lam.Identifier:
        return pi.Write(parent, l_ast.val, pi.End())
    if type(l_ast) == lam.Definition:
        func = new_channel("func")
        ret = new_channel('ret')
        return pi.New(func,
                pi.SpawnForever(
                    pi.Read(func,
                     pi.Tuple('x',ret),
                     translate_term(l_ast.body, ret)),
                    pi.Write(parent,func,pi.End())))
    if type(l_ast) == lam.Application:
        left = new_channel("left")
        right = new_channel("right")
        return pi.New(left,
                pi.Spawn(translate_term(l_ast.function, left), 
                 pi.New(right,
                  pi.Spawn(translate_term(l_ast.argument, right),
                   pi.Spawn(
                    pi.Read(right, 'arg',
                     pi.Read(left, 'func',
                      pi.Write('func',
                       pi.Tuple('arg',parent),
                       pi.End()))),
                    pi.End())))))
    raise Exception(type(l_ast))

print(translate_program('5'))
print(translate_program('(^ x x)'))
print(translate_program('(^ x 5)'))
print(translate_program('(^ x (^ y y))'))
print(translate_program('((^ x x) 5)'))
class TranslateTest(unittest.TestCase):
    def test_num(self):
        self.assertEqual(
                translate_program('5'),
                "new(Gout).Gout!5.0")

if __name__ == '__main__':
    unittest.main()
