# Automatic Parallelization

This repository has several related projects in it.  Each sub-project has a sub-section below.

## Lambda to pi calculus converter

Python script to convert programs written in lambda calculus into programs written in pi calculus.

## Papa Murphy's

Macro system for python written using RedBaron.  Does simple nonrecursive code transformations based on pattern matching and symbol substitution.

## Dominos

Automatic parallelization of loops in python programs using Papa Murphy's as a code transformation library. "@Parallel" must be present in the line preceding the list comprehension for parallelization to be attempted.

**Usage:** python3 dominos.py [a python file] [a series of command line inputs to test with]

**Example usage:** python3 dominos.py test.py "arg1 arg2 arg3" "arg4" ""


## Breadsticks

Automatic parallelization of function calls in python programs using RedBaron as a code transformation library. "@Parallel" must be present in the line preceding the function call for parallelization to be attempted.

**Usage:** python3 breadsticks.py [a python file]